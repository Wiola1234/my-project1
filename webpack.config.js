var Encore = require('@symfony/webpack-encore');

Encore
    .autoProvidejQuery()
    // directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // public path used by the web server to access the output path
    .setPublicPath('/build')
    // only needed for CDN's or sub-directory deploy
    //.setManifestKeyPrefix('build/')
    .createSharedEntry('vendor', [
        'jquery',
    ])

    /*
     * ENTRY CONFIG
     *
     * Add 1 entry for each "page" of your app
     * (including one that's included on every page - e.g. "app")
     *
     * Each entry will result in one JavaScript file (e.g. app.js)
     * and one CSS file (e.g. app.css) if you JavaScript imports CSS.
     */
    .addEntry('app', './assets/js/app.js')
    .addStyleEntry ('main', './assets/css/main.css')
    .addStyleEntry ('responsive', './assets/css/responsive.css')
    .addStyleEntry ('roboto-webfont', './assets/css/roboto-webfont.css')
    .addStyleEntry ('style', './assets/css/style.css')
    .addStyleEntry ('bootstrap.min', './assets/css/bootstrap.min.css')
    .addStyleEntry ('plugins', './assets/css/plugins.css')
    .addEntry('js/main', './assets/js/main.js')
    .addEntry('js/modernizr', './assets/js/modernizr.js')
    .addEntry('js/plugins', './assets/js/plugins.js')
    .addEntry('js/bootstrap.min', './assets/js/vendor/bootstrap.min.js')
    .addEntry('js/jquery-1.11.2.min', './assets/js/vendor/jquery-1.11.2.min.js')
    .addEntry('js/modernizr-2.8.3-respond-1.4.2.min', './assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js')


    //.addEntry('page1', './assets/js/page1.js')
    //.addEntry('page2', './assets/js/page2.js')

    /*
     * FEATURE CONFIG
     *
     * Enable & configure other features below. For a full
     * list of features, see:
     * https://symfony.com/doc/current/frontend.html#adding-more-features
     */
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    // enables hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())

    // enables Sass/SCSS support
    //.enableSassLoader()

    // uncomment if you use TypeScript
    //.enableTypeScriptLoader()

    // uncomment if you're having problems with a jQuery plugin
    //.autoProvidejQuery()
;

module.exports = Encore.getWebpackConfig();
