<?php
/**
 * Created by PhpStorm.
 * User: Wioleta
 * Date: 22.08.2018
 * Time: 12:26
 */

// src/Controller/LuckyController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class LuckyController extends AbstractController
{
    /**
     * @Route("/lucky/{number}",  name="app_lucky", requirements={"number"="\d+"})
     */
    public function number(int $number = 7)
    {
//        $number = random_int(0, 100);

        return $this->render('lucky/number.html.twig', [
            'number' => $number,
        ]);
    }

    /**
     * @Route("/count/{a}{sign}{b}", name="app_count")
     */
    public function calculator(int $a, $sign, int $b)
    {
        $result = 999;
        switch ($sign) {
            case '+':
                $result = $a + $b;
                break;
            case '-':
                $result = $a - $b;

                break;
            case '*':
                $result = $a * $b;
                break;
            case '/':
                if ($b == 0) {
                    echo("Nie dziel przez zero!");
                } else {
                    $result = $a / $b;
                }

        }
        return new Response(
            "<html><body>Wynik dzialania to: $result </body></html>");
    }

    /**
     * @Route("/", name="app_home")
     */
    public function home()
    {
        return $this->render('index.html.twig');

    }
}
